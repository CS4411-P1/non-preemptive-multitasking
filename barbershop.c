/*
 * barbershop.c:
 *      Your comments go here
 *      
 *
 *
 */
#include <stdlib.h>
#include <stdio.h>
#include "minithread.h"
#include "queue.h"
#include "synch.h"
#include "random.h"

#include <assert.h>

#define SHOP_CAPACITY 2
#define BARBERS_MAX 3
#define CUSTOMERS_MAX 7
#define BARBER_ACTIVATE_SEMA 0
#define BARBER_WAIT_SEMA 1
#define TRUE 1



typedef struct barber barber_t;
/*
* Structure definition for barber type
*/
struct barber {
	int id;
	semaphore_t * activateSema;//Semaphore that blocks and activates barber.Initialized to 0
	semaphore_t * waitSema;//Semaphore that blocks customers waiting for a barber. Initialized to 1
};
/*
* Structure definition for customer type
*/
typedef struct customer customer_t;

struct customer {
	int id;
	int barber_id;
};


semaphore_t * shop_capacity = NULL; //Semaphore to block customers if shop is fully occupied
barber_t  barbers[BARBERS_MAX];
customer_t customers[CUSTOMERS_MAX];

int customer_thread(int* id){
	printf("Customer %d requested barber %d\n",*id,customers[*id].barber_id);
	printf("customer %d OUTSIDE\n",*id);
	semaphore_P(shop_capacity);
	printf("customer %d INSIDE\n",*id);
	semaphore_P(barbers[customers[*id].barber_id].waitSema);
	printf("Barber %d STARTING HAIR CUT of Customer %d\n",customers[*id].barber_id,*id);
	semaphore_V(barbers[customers[*id].barber_id].activateSema);
	minithread_yield();
	
	return 0;
}

int barber_thread(int* id){
	while(TRUE){
		//block self - wake up when customer comes
		semaphore_P(barbers[*id].activateSema);
		printf("Barber %d CUTTING HAIR\n",*id);
		minithread_yield();
		printf("Barber %d FINISHED JOB\n",*id);	
		semaphore_V(barbers[*id].waitSema);
		semaphore_V(shop_capacity);
	}
return 0;
}

int simulate_shop(int* arg){
	int i;
	
	//Initialize capacity semaphores
	shop_capacity = semaphore_create();
 	semaphore_initialize(shop_capacity,SHOP_CAPACITY);
 	
	//Initialize barber structures and threads
	for(i=0; i < BARBERS_MAX;i++){
		barbers[i].id = i;
		barbers[i].activateSema = semaphore_create();
		semaphore_initialize(barbers[i].activateSema,BARBER_ACTIVATE_SEMA);
		barbers[i].waitSema = semaphore_create();
		semaphore_initialize(barbers[i].waitSema,BARBER_WAIT_SEMA);
		minithread_fork(barber_thread,&(barbers[i].id));
	}
	
	//Iniitialize customer threads
	for(i=0; i < CUSTOMERS_MAX;i++){
		customers[i].id = i;
		customers[i].barber_id = genintrand(BARBERS_MAX)-1;
		minithread_fork(customer_thread,&(customers[i].id));
	}
	
	return 0;	
}

int main(void) {

  printf("The barbershop is starting up for business!\n");
  minithread_system_initialize(simulate_shop,NULL);
  return 0;
}


