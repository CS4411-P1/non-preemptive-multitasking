
/*
 * minithread.c:
 *      This file provides a few function headers for the procedures that
 *      you are required to implement for the minithread assignment.
 *
 *      EXCEPT WHERE NOTED YOUR IMPLEMENTATION MUST CONFORM TO THE
 *      NAMING AND TYPING OF THESE PROCEDURES.
 *
 */
#include <stdlib.h>
#include <stdio.h>
#include "minithread.h"
#include "queue.h"
#include "synch.h"

#include <assert.h>

/*
 * A minithread should be defined either in this file or in a private
 * header file.  Minithreads have a stack pointer with to make procedure
 * calls, a stackbase which points to the bottom of the procedure
 * call stack, the ability to be enqueueed and dequeued, and any other state
 * that you feel they must have.
 */

#define noop	//No operation

//Global vars
int nextThreadID;
queue_t *readyQueue;
queue_t *zombieQueue;
minithread_t *runningThreadPointer;
minithread_t *idleThread;
minithread_t *reaperThread;
minithread_t *kernelThread;
semaphore_t *waitSem;	//Semaphore for reaper thread

typedef enum status status_t;

enum status { NEW,
	WAITING,
	RUNNABLE,
	RUNNING,
	ZOMBIE
};

struct minithread {
	stack_pointer_t stacktop;
	stack_pointer_t stackbase;
	int threadID;
	status_t status;
};

/* 'Final' cleanup function */
int final_proc(int *threadID){
	runningThreadPointer->status = ZOMBIE;
	queue_append(zombieQueue, runningThreadPointer);	//Add to zombie queue
	semaphore_V(waitSem);	//Increments semaphore or makes waiting thread runnable, but no context switch
	minithread_stop();	//Context switch so final_proc never returns
	return 0;
}

int idle_proc(int *unused){
	printf("\nIn idle thread");		//Print for grader assistance
	printf("\nIn idle thread");		//Statement gets skipped in execution
	while(1);
	return 0;
}

int reaper_proc(int *sem){
	while(1){
		if (zombieQueue == NULL){
			noop;
		}	
		else{
			void *current = NULL;
			queue_dequeue(zombieQueue, &current);	//Get first zombie
			while(current != NULL){		//Go through zombie list
				minithread_free_stack(((minithread_t*)current)->stackbase);	//Free zombie TCB stack
				queue_dequeue(zombieQueue, &current);
			}		
		}	//End else
		semaphore_P(waitSem);
	}	//End while
	return 0;
}

/* minithread functions */

minithread_t*
minithread_fork(proc_t proc, arg_t arg) {
	minithread_t* minithreadPointer = minithread_create(proc,arg);
	minithread_start(minithreadPointer);
    return minithreadPointer;
}

minithread_t*
minithread_create(proc_t proc, arg_t arg) {
    minithread_t* minithreadPointer = (minithread_t*) malloc(sizeof(minithread_t));
	assert(minithreadPointer != NULL);
	minithread_allocate_stack(&minithreadPointer->stackbase,&minithreadPointer->stacktop);
	minithread_initialize_stack(&minithreadPointer->stacktop, proc, arg, final_proc, NULL);
	minithreadPointer->status = NEW;
	minithreadPointer->threadID = nextThreadID;
	nextThreadID++;		//Thread IDs assigned sequentially
    return minithreadPointer;
}

minithread_t*
minithread_self() {
    return runningThreadPointer;
}

int
minithread_id() {
    return runningThreadPointer->threadID;
}

void
minithread_stop() {
	void *temp = NULL;
	queue_dequeue(readyQueue,&temp);	//Check reaady queue
	if (temp == NULL || temp == runningThreadPointer){	//Ready queue was empty or had lone thread, switch to idle thread
		temp = kernelThread;
	}
	minithread_t *readyThread = (minithread_t*) temp;
	temp = runningThreadPointer;	//Copying pointer for switching
	runningThreadPointer = readyThread;
	readyThread->status = RUNNING;
	minithread_switch(&(((minithread_t*)temp)->stacktop), &(readyThread->stacktop));	//Context switch. temp is old thread
}

void
minithread_start(minithread_t *t) {
	t->status = RUNNABLE;
	queue_append(readyQueue, t);	//Add to runnable queue
}

void
minithread_yield() {
	runningThreadPointer->status = RUNNABLE;
	queue_append(readyQueue, runningThreadPointer);	//send stopped thread to back of queue
	minithread_stop();
}

void
minithread_system_initialize(proc_t mainproc, arg_t mainarg) {
	//Create ready and zombie queues
	readyQueue = queue_new();
	assert(readyQueue != NULL);
	zombieQueue = queue_new();
	assert(zombieQueue != NULL);

	nextThreadID = 0;

	waitSem = semaphore_create();
	semaphore_initialize(waitSem,0);

	//Create kernel thread TCB
	kernelThread = (minithread_t*) malloc(sizeof(minithread_t));	//Kernel thread does not use minithread_create as we do not allocate stack to it
	kernelThread->stacktop = (stack_pointer_t) malloc(sizeof(int));	//Address space size is int?
	kernelThread->stacktop = NULL;
	ernelThread->stackbase = (stack_pointer_t) malloc(sizeof(int));
	kernelThread->stackbase = NULL;
	kernelThread->threadID = nextThreadID;
	nextThreadID++;		//Thread IDs assigned sequentially
	kernelThread->status = RUNNING;
	runningThreadPointer = kernelThread;

	//Create main thread
	minithread_fork(mainproc, mainarg);

	//Create reaper thread
	reaperThread = minithread_fork(reaper_proc,NULL);

	//Create idle thread
	idleThread = minithread_create(idle_proc,NULL);

	minithread_stop();	//Switch to main thread, make sure kernel thread never ends

	while(1);	//Use kernel thread as idle thread
}


