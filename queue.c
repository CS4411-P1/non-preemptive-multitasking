/*
 * Generic queue implementation.
 *
 */
#include "queue.h"
#include <stdlib.h>
#include <stdio.h>

/*
 * queueNode_t is a generic queue Node type 
 */
typedef struct queueNode queueNode_t; 

/*
 * Structure for generic queue node 
 */
struct queueNode {
	void *value;
	queueNode_t *next;
};

/*
 * Structure for queue type
 */
struct queue {
	queueNode_t *head;
	queueNode_t *tail;
};

/*
 * Return an empty queue.  Returns NULL on error.
 */
queue_t* queue_new() {
    queue_t* queuePointer = (queue_t*) malloc(sizeof(queue_t));
    //return NULL if malloc error happened
    if(queuePointer == NULL)
		return NULL;
	queuePointer->head = NULL;
	queuePointer->tail = NULL;
	return queuePointer;	
}

/*
 * Prepend a void* to a queue (both specifed as parameters).
 * Returns 0 (success) or -1 (failure).
 */
int queue_prepend(queue_t *queue, void* item) {
	//Check if both pointers are not null
	if(queue == NULL || item == NULL)
		return -1;
	//Allocate memory to new Node and add value	
	queueNode_t *newNode = (queueNode_t*)malloc(sizeof(queueNode_t));
	if(newNode == NULL)
		return -1;
	newNode->value = item;
	//Prepend to empty queue
	if(queue->head == NULL && queue->tail == NULL){
		newNode->next = NULL;
		queue->head = newNode;
		queue->tail = newNode;
		return 0;
	}
	newNode->next = queue->head;
	queue->head = newNode;
    return 0;
}

/*
 * Appends a void* to a queue (both specifed as parameters).  Return
 * 0 (success) or -1 (failure).
 */
int queue_append(queue_t *queue, void* item) {
	//Check if both pointers are not null
	if(queue == NULL || item == NULL)
		return -1;
	//Allocate memory to new Node and add value	
	queueNode_t *newNode = (queueNode_t*)malloc(sizeof(queueNode_t));
	if(newNode == NULL)
		return -1;
	newNode->value = item;
	newNode->next = NULL;
	//Append to empty queue
	if(queue->head == NULL && queue->tail == NULL){
		queue->head = newNode;
		queue->tail = newNode;
		return 0;
	}
	queue->tail->next = newNode;
	queue->tail = newNode;	
    return 0;
}

/*
 * Dequeue and return the first void* from the queue.
 * Return 0 (success) and first item if queue is nonempty, or -1 (failure) and
 * NULL if queue is empty.
 */
int queue_dequeue(queue_t *queue, void** item) {
		
	//Empty case
	if(queue == NULL || (queue->head == NULL && queue->tail == NULL)){
		*item = NULL;
		return -1;
	}
	
	*item = queue->head->value;
	queueNode_t *toFree = queue->head;
	//Single Node case
	if(queue->head == queue->tail){
		free(toFree);
		queue->head = NULL;
		queue->tail = NULL;
		return 0;
	}
	
	queue->head = queue->head->next;
	free(toFree);
	return 0;
}

/*
 * queue_iterate(q, f, t) calls f(x,t) for each x in q.
 * q and f should be non-null.
 *
 * returns 0 (success) or -1 (failure)
 */
int queue_iterate(queue_t *queue, func_t f, void* item) {
	if(queue == NULL || f == NULL || queue->head == NULL)
		return -1;
	
	queueNode_t* current = queue->head;
	while(current != NULL){
		f(current->value,item);
		current = current-> next;
	}
	return 0;
}

/*
 * Free the queue and return 0 (success) or -1 (failure).
 */
int queue_free (queue_t *queue) {
	if(queue == NULL)
		return -1;
	queueNode_t* current = queue->head;
	queueNode_t* previous = NULL;
	while(current != NULL){
		previous = current;
		current = current->next;
		free(previous);
	}
	free(queue);
    return 0;
}

/*
 * Return the number of items in the queue, or -1 if an error occured
 */
int queue_length(const queue_t *queue){
    if(queue == NULL)
		return -1;
	
	int length = 0;
	queueNode_t* current = queue->head;
	while(current != NULL){
		length++;
		current = current->next;
	}
	return length;
}

/*
 * Delete the first instance of the specified item from the given queue.
 * Returns 0 if an element was deleted, or -1 otherwise.
 */
int queue_delete(queue_t *queue, void* item) {
	if(queue == NULL)
		return -1;
	queueNode_t* current = queue->head;
	queueNode_t* previous = NULL;
	while(current != NULL && current->value != item){
		previous = current;
		current = current->next;
	}
	//item not found in queue
	if(current == NULL)
		return -1;
	//single item in queue
	if(current == queue->head && current == queue->tail){
		free(current);
		queue->head = NULL;
		queue->tail = NULL;
		return 0;
	}
	//item to be deleted in the front
	else if(current == queue->head){
		queue->head = current->next;
		free(current);
		return 0;
	}
	//item to be deleted in the end
	else if(current == queue->tail){
		previous->next = current->next;
		queue->tail = previous;
		free(current);
		return 0;
	}
	
	previous->next = current->next;
	free(current);
	
    return 0;
}
