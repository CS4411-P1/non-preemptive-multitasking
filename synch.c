#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "defs.h"
#include "synch.h"
#include "queue.h"
#include "minithread.h"

#define MAX_THREADS INT_MAX

/*
 *      You must implement the procedures and types defined in this interface.
 */


/*
 * Semaphores.
 */
struct semaphore {
    queue_t* waitQ ;
    int count;
};

/*
 * semaphore_t semaphore_create()
 *  Allocate a new semaphore.
 */
semaphore_t* semaphore_create() {
    semaphore_t* semaphorePointer = (semaphore_t*) malloc(sizeof(semaphore_t));
    //return NULL if malloc error happened
    if(semaphorePointer == NULL)
		return NULL;
	//return if queue could not be allocated
	semaphorePointer->waitQ = queue_new();
	if(semaphorePointer->waitQ == NULL)
		return NULL;
	semaphorePointer->count = MAX_THREADS;
	return semaphorePointer;
}

/*
 * semaphore_destroy(semaphore_t sem);
 *  Deallocate a semaphore.
 */
void semaphore_destroy(semaphore_t *sem) {
	if(sem != NULL){
		//Free Semaphore Queue
		queue_free(sem->waitQ);
		//Free semaphoore
		free(sem);
	}
}

/*
 * semaphore_initialize(semaphore_t sem, int cnt)
 *  initialize the semaphore data structure pointed at by
 *  sem with an initial value cnt.
 */
void semaphore_initialize(semaphore_t *sem, int cnt) {
	if(sem != NULL){
		sem->count = cnt;
	}
}

/*
 * semaphore_P(semaphore_t sem)
 *  P on the sempahore.
 */
void semaphore_P(semaphore_t *sem) {
	if(sem->count > 0)
		sem->count -= 1;
	else{
		queue_append(sem->waitQ, minithread_self());
		minithread_stop();
		
	}
}

/*
 * semaphore_V(semaphore_t sem)
 *  V on the sempahore.
 */
void semaphore_V(semaphore_t *sem) {
	if (queue_length(sem->waitQ) == 0) 
		sem->count += 1;
	else {
		assert(sem->count == 0);
		void* t = NULL;
		queue_dequeue(sem->waitQ, &t);
		minithread_start((minithread_t*)t); // sets status to RUNNABLE
		}
}

